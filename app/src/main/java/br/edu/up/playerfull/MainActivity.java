package br.edu.up.playerfull;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {

  MediaPlayer player;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  public void executarMusicaArquivo(View v) {
    player = MediaPlayer.create(this, R.raw.sambo_sunday_bloody_sunday);
    player.start();
  }

  public void executar(View v) {
    if(!player.isPlaying()) {
      player.start();
    }
  }
  public void pausar(View v) {
    if(player.isPlaying()) {
      player.pause();
    }
  }
  public void parar(View v) {
    if(player.isPlaying()) {
      player.stop();
    }
  }

  @Override
  protected void onStop() {
    super.onStop();
    liberarPlayer();
  }
  private void liberarPlayer() {
    if(player != null){
      player.release();
    }
  }

  public void executarMusicaUrl(View v) {
    liberarPlayer();
    player = new MediaPlayer();
    try {
      Uri uri = Uri.parse("http://tylergrund.com/mp3/Beatles/Imagine.mp3");
      player.setDataSource(this, uri);
      player.setAudioStreamType(AudioManager.STREAM_MUSIC);
      player.setOnPreparedListener(this);
      player.prepareAsync();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void onPrepared(MediaPlayer mp) {
    player.start();
  }
}
